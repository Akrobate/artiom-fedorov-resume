# Online CV

## Installing and working localy

You will need to perform the next commands

'''bash
docker build -t jekyll .
docker run --volume `pwd`:/public -p 4000:4000 -it jekyll bash -c "bundler exec jekyll serve"

'''

Simplest way, you can just run start.dev.sh

'''bash
./start.dev.sh
'''

## Filling data

Change template configuration in: ``_config.yml``
Change all the details from one place: ``_data/data.yml``

## Credits

Template forked from [github.com/sharu725/online-cv](https://github.com/sharu725/online-cv)
Thanks to [sharath Kumar](https://github.com/sharu725)

## License

This project is licensed under the [MIT license](LICENSE.txt).
