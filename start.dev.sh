#!/bin/bash

# docker run --volume /home/home/projects/artiom-fedorov-resume:/public -p 4000:4000 -it jekyll bash

docker build -t jekyll .
docker run --volume `pwd`:/public -p 4000:4000 -it jekyll bash -c "bundler exec jekyll serve"
